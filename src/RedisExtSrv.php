<?php
// +----------------------------------------------------------------------
// | 按phpredis扩展的方式调用redis
// +----------------------------------------------------------------------
// | Author: zhuqi
// +----------------------------------------------------------------------
namespace Cheer\Redis;

use Redis;
class RedisExtSrv  {

    public $connected_status = true;
    /**
     +----------------------------------------------------------
     * 架构函数
     +----------------------------------------------------------
     * @access public
     +----------------------------------------------------------
     */
    public function __construct ($options = array()) {
        if ( !extension_loaded('redis') ) {
            $this->connected_status = false;
            return false;
        }
        if (empty($options)) {
            throw new \Exception('Not Found Redis Conf');
        }
        $this->options =  $options;
        $func = $options['persistent'] ? 'pconnect' : 'connect';
        $this->handler  = new Redis();
        try {
        	if(empty($options['timeout'])){
        		$this->handler->$func($options['host'], $options['port']);
        	}else{
        		$this->handler->$func($options['host'], $options['port'], intval($options['timeout']));
        	}
            $this->handler->auth($options['password']);
            
            if(empty($options['database'])){
            	$this->handler->select(0);
            }else {
            	$this->handler->select($options['database']);
            }
        } catch (\RedisException $e) {
        	$errorMsg = '错误信息:'.$e->getMessage().';';
        	$errorMsg .= '连接方法:'.$func.';';
        	$errorMsg .= '连接配置:'.json_encode($options).';';
        	
        	throw new \RedisException($errorMsg);
        }
    }

    
    /**
     +----------------------------------------------------------
     * 调用redis扩展本身的方法
     +----------------------------------------------------------
     * @access public
     +----------------------------------------------------------
     */
    public function __call($function_name, $args){
    	return call_user_func_array(array($this->handler, $function_name), $args);
    }
    
    

    /**
     +----------------------------------------------------------
     * 删除缓存
     *
     +----------------------------------------------------------
     * @access public
     +----------------------------------------------------------
     * @param string $name 缓存变量名
     +----------------------------------------------------------
     * @return boolen
     +----------------------------------------------------------
     */
    public function rm($name) {
        return $this->handler->delete($name);
    }

        /**
     +----------------------------------------------------------
     * 删除缓存
     *
     +----------------------------------------------------------
     * @access public
     +----------------------------------------------------------
     * @param string $name 缓存变量名
     +----------------------------------------------------------
     * @return boolen
     +----------------------------------------------------------
     */
    public function rm4Keys($name) {
        $keyArray = $this->keys($name.'*');
        if (is_array($keyArray) && count($keyArray)){
            foreach ($keyArray as $key=>$value){
                $this->handler->delete($value);
            }
        }
        return TRUE;
    }
    
    /**
     +----------------------------------------------------------
     * 清除缓存
     +----------------------------------------------------------
     * @access public
     +----------------------------------------------------------
     * @return boolen
     +----------------------------------------------------------
     */
    public function clear() {
        return $this->handler->flushDB();
    }

   
    
    
    
    public function keys($key='*'){
    	return $this->handler->keys($key);
    }
    
}