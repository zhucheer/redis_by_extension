<?php

namespace Cheer\Redis;

use Illuminate\Support\ServiceProvider;

class RedisExtProvider extends ServiceProvider
{
	
	//protected $defer = true;
	
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
     
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    	//注册后台文件上传服务
    	$this->app->singleton('redisExt', function ($app) {
    		$redis_conf = config('database.redis.default');
    		
    		return new RedisExtSrv($redis_conf);
    	});
    	
    }
}
