#通过redis的php扩展来连接redis,效率要比laravel带的Predis高

>安装
- 编辑composer.json 在require中添加 "cheer/redis_by_extension": "dev-master" 然后之前composer update即可


>使用方法

- 在app.php的providers节点下增加以下代码

```
Cheer\RedisExtProvider::class
```
或
```
'Cheer\RedisExtProvider'
```

- 在app.php的aliases节点下增加以下代码

```
'RedisExt' => Cheer\RedisExtFacade::class
```
或
```
'RedisExt' => 'Cheer\RedisExtFacade'
```

- redis配置读取config/database.php下

```
'redis' => [
		'default' => [
			'host'     => '127.0.0.1',
			'port'     => 6379,
			'database' => 0,
            'password' => '123456',
            'persistent' => true,//是否长连接
            'timeout'	=> 300,//连接时长
		],
		
		....
```